/**
 * ValidateroomController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  	validate: (req,res) =>{
		var values = req.allParams();
		if(values.roomId!=''){
			Rooms.find({'roomId':values.roomId,'password':values.password}).limit(1).exec((err,records)=>{
				if(err){
					res.send(500,err);
				}
				var count=records.length;
				res.ok({'status':count>0?true:false});
			});
		}else{
			res.send(500);
		}
  		
	},
};

