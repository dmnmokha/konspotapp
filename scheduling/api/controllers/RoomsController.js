/**
 * RoomsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
	list: (req,res) =>{
		Rooms.find({'addedBy':req.session.userId}).sort('id DESC').exec((err,records)=>{
			if(err){
				res.send(500,err);
			}
			return res.view('rooms/create-room', {'records':records});
		});
  		
	},
	add: function (req,res){
		var values = req.allParams();
		//alert(values.title);
		var bufferText = Buffer.from(values.time+' '+values.title, 'utf8');
		var roomId=bufferText.toString('hex');
		Rooms.create({'title':values.title,'password':values.password,'roomId':roomId,'start':values.date+' '+values.start,'end':values.date+' '+values.end,'duration':values.duration,'addedBy':req.session.userId,'url':'https://meet.teckvisor.com/meeting.html?roomid='+roomId+'&password='+values.password},(err,room)=>{
			if(err){
	            return res.send(500,err);
	        }
	        Rooms.find({'addedBy':req.session.userId}).exec((err,records)=>{
			if(err){
					res.send(500,err);
				}
				//return res.view('rooms/create-room', {'records':records});
				return res.redirect('/');
			});
		});
		
	},
	newmeeting: function (req,res){
		var values = req.allParams();
		//alert(values.title);
		let date_ob = new Date();
		let date = ("0" + date_ob.getDate()).slice(-2);
		let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
		let year = date_ob.getFullYear();
		let hours = date_ob.getHours();
		let minutes = date_ob.getMinutes();
		var currentDate=year + "-" + month + "-" + date + " " + hours +":"+minutes;

		var bufferText = Buffer.from(currentDate, 'utf8');
		var roomId=bufferText.toString('hex');
		var url='https://meet.teckvisor.com/meeting.html?roomid='+roomId+'&password=';
		Rooms.create({'title':values.title,'roomId':roomId,'start':currentDate,'addedBy':req.session.userId,'url':url},(err,room)=>{
			if(err){
	            return res.send(500,err);
	        }
	        Rooms.find({'addedBy':req.session.userId}).sort('id DESC').exec((err,records)=>{
			if(err){
					res.send(500,err);
				}
				//return res.view('rooms/create-room', {'records':records});
				return res.redirect(url);
			});
		});
		
	},
	validate: (req,res) =>{
		var values = req.allParams();
		Rooms.find({'roomId':values.roomId,'password':values.password}).exec((err,records)=>{
			if(err){
				res.send(500,err);
			}
			return res.ok({'records':records});
		});
  		
	},
	scheduled: (req,res) =>{
		Rooms.find({'addedBy':req.session.userId}).sort('id DESC').exec((err,records)=>{
			if(err){
				res.send(500,err);
			}
			//var recs=JSON.stringify(records);
			return res.view('rooms/scheduled-meetings', {records:records});
		});
		
	},
	scheduledroom: (req,res) =>{
		Rooms.find({'addedBy':req.session.userId}).exec((err,records)=>{
			if(err){
				res.send(500,err);
			}
			
			return res.ok({records});
		});
  		
	},
};

