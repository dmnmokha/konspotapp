/**
 * Rooms.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
    roomId: {
      type: 'string',
      required: true,
      unique: true,
      maxLength: 200,
      example: 'Room1'
    },
    password: {
      type: 'string',
      required: false,
      description: 'Securely hashed representation of the user\'s login password.',
      protect: true,
      example: '2$28a8eabna301089103-13948134nad'
    },
    start: {
        type: 'ref',
        columnType:'datetime',
        required: false,
        example: '2020-07-01 01:23:34'
    },
    title: {
        type: 'string',
        required: true,
        example: 'Room 1'
    },
    url: {
        type: 'string',
        required: true,
        example: 'https://meet.teckvisor.com?roomid=3245&password=123456'
    },
    addedBy: {
        type: 'number',
        required: true,
        example: '1'
    },
    end: {
        type: 'ref',
        columnType:'datetime',
        required: false,
        example: '2020-07-01 01:23:34'
    },

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

  },

};

